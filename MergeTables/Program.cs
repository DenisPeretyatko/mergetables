﻿using MergeTables.DbContext;
using MergeTables.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace MergeTables
{
    class Program
    {
        public static IConfiguration Configuration { get; set; }
        public static object ConfigurationManager { get; private set; }

        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();
            MergeTables();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            Configuration = new ConfigurationBuilder().Build();
            VituDbContext.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }

        public static void MergeTables()
        {
            Console.WriteLine("Start Processing");
            List<FeeQuoteLookupRecord> feeQuotes = MergeService.Instance.GetFeeQuote();
            Console.WriteLine("FeeQuotes Loaded");
            List<FeeDTO> feeDTOs = MergeService.Instance.CreateFeeRecords(feeQuotes);
            List<FeeResponseDTO> feeResponseDTOs = MergeService.Instance.CreateFeeResponseRecords(feeQuotes);
            Console.WriteLine("DTO objects created");
            MergeService.Instance.SaveFees(feeDTOs);
            Console.WriteLine("FeeDTO inserted");
            MergeService.Instance.SaveFeeQuoteResponses(feeResponseDTOs);
            Console.WriteLine("FeeResponseDTO inserted");
            Console.WriteLine("End work");
        }
    }
}
