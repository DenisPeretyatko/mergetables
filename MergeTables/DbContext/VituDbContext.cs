﻿using System.Data;
using Npgsql;
using MVSC.Framework;

namespace MergeTables.DbContext
{
    public abstract class VituDbContext
    {
        private static PgSqlDatabaseProvider _provider;
        public static string ConnectionString;

        protected static PgSqlDatabaseProvider Provider
        {
            get
            {
                if (_provider == null)
                {
                    _provider = new PgSqlDatabaseProvider(ConnectionString);
                }
                return _provider;
            }
        }

        public static IDbConnection OpenConnection()
        {
            IDbConnection connection = new NpgsqlConnection(ConnectionString);
            connection.Open();
            return connection;
        }
    }
}
