﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MergeTables.Models
{
    public class FeeDTO
    {
        public int transactionId { get; set; }
        public decimal amount { get; set; }
        public string description { get; set; }
    }
}
