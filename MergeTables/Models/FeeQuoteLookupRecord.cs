﻿using System;
using System.Collections.Generic;
using System.Data;
using MVSC.Framework;
using Newtonsoft.Json;

namespace MergeTables.Models
{
    public class FeeQuoteLookupRecord
    {
        public int transactionId;
        public string documentUrl;
        public List<FormsAndFeesFeeRecord> fees;
        public string error;

        public FeeQuoteLookupRecord() { }

        public FeeQuoteLookupRecord(IDataReader record)
        {
            transactionId = record.GetInt("id");
            documentUrl = record.GetStringNullable("documentUrl");
            fees = record.GetStringNullable("fees") != null? JsonConvert.DeserializeObject<List<FormsAndFeesFeeRecord>>(record.GetStringNullable("fees")) : new List<FormsAndFeesFeeRecord>();
            error = record.GetStringNullable("error");
        }
    }
}
