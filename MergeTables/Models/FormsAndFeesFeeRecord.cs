﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MergeTables.Models
{
    public class FormsAndFeesFeeRecord
    {
        public decimal amount;
        public string description;

        public FormsAndFeesFeeRecord() { }

        public FormsAndFeesFeeRecord(decimal amount, string description)
        {
            this.amount = amount;
            this.description = description;
        }
    }
}
