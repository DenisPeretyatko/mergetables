﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MergeTables.Models
{
    public class FeeResponseDTO
    {
        public int transactionId { get; set; }
        public string documentUrl { get; set; }
        public string error { get; set; }
        public bool issuccess { get; set; }
    }
}
