﻿using MergeTables.DbContext;
using MergeTables.Models;
using MVSC.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MergeTables
{
    public class MergeService : VituDbContext
    {
        private static readonly Lazy<MergeService> _lazy = new Lazy<MergeService>(() => new MergeService());
        public static MergeService Instance => _lazy.Value;
        public List<FeeQuoteLookupRecord> GetFeeQuote()
        {
            const string query = @"select * from FeeQuote";

            List<FeeQuoteLookupRecord> records = new List<FeeQuoteLookupRecord>();
            using (IDataReader reader = Provider.ExecuteReader(query))
            {
                while (reader.Read())
                {
                    records.Add(new FeeQuoteLookupRecord(reader));
                }
                reader.Close();
            }
            return records;
        }

        public void SaveFees(List<FeeDTO> fees)
        {
            using (IDbConnection connection = Provider.OpenConnection())
            {
                foreach (var fee in fees)
                {
                    SaveFee(fee);
                };
            }
        }

        public void SaveFee(FeeDTO fee)
        {
            string query = @"
				insert into
					Fee
				(
					id,
					amount,
					description
				)
                values
				(
					@id,
					@amount,
					@description
				)";

                QueryParamList paramList = new QueryParamList();
                paramList.Add("id", fee.transactionId);
                paramList.Add("amount", fee.amount);
                paramList.Add("description", fee.description);

            Provider.ExecuteNonQuery(query, paramList);
        }

        public void SaveFeeQuoteResponses(List<FeeResponseDTO> feeResponses)
        {
            using (IDbConnection connection = Provider.OpenConnection())
            {
                foreach (var feeResponse in feeResponses)
                {
                    SaveFeeQuoteResponse(feeResponse);
                };
            }
        }
        public void SaveFeeQuoteResponse(FeeResponseDTO feeResponse)
        {
            string query = @"
				insert into
					FeeQuoteResponse
				(
					id,
					issuccess,
					error,
					documenturl,
				)
                values
				(
					@id,
					@issuccess,
					@error,
					@documenturl
				)";

                QueryParamList paramList = new QueryParamList();
                paramList.Add("id", feeResponse.transactionId);
                paramList.Add("issuccess", feeResponse.issuccess);
                paramList.Add("error", feeResponse.error);
                paramList.Add("documenturl", feeResponse.documentUrl);

            Provider.ExecuteNonQuery(query, paramList);
        }

        public List<FeeDTO> CreateFeeRecords(List<FeeQuoteLookupRecord> fees) {
            List<FeeDTO> feeDTOs = new List<FeeDTO>();

            foreach (var fee in fees)
            {
                var expandedFees = fee.fees?.Select(f => 
                    new FeeDTO { transactionId = fee.transactionId, amount = f.amount, description = f.description }
                    ).ToList();
                feeDTOs.AddRange(expandedFees);
            };
            return feeDTOs;
        }

        public List<FeeResponseDTO> CreateFeeResponseRecords(List<FeeQuoteLookupRecord> feeResponses)
        {
            List<FeeResponseDTO> feeResponseDTOs = new List<FeeResponseDTO>();

            feeResponseDTOs = feeResponses?.Select(f => new FeeResponseDTO {
                transactionId = f.transactionId,
                issuccess = string.IsNullOrEmpty(f.error)? false : true,
                error = f.error,
                documentUrl = f.documentUrl
            }).ToList();
            return feeResponseDTOs;
        }
    }
}
